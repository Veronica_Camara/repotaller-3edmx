var clientesObtenidos;

function getClientes() {
  //Cuando tenemos una cadena porque viene de un campo lo ponemos en comilla simple o apostrofe
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  // para documentar esta libreria envia peticiones HTTP hacia una api
  /*Asi documento vvarios renglones*/
  var request = new XMLHttpRequest();

  request.onreadystatechange = function () {
    // Aqui se valida el estado ! = Preparando 2= Esperando 3 = enviando respuesta
    //4 Respondio y la respuesta de la petición es 200
    if (this.readyState == 4 && this.status == 200) {
/*siempre traer los datos a consola para validar que funciona ok y
se está mandando a una tabla dentro de la consola siempre se debe hacer el jason con parse */
  //   console.table(JSON.parse(request.responseText).value);
     clientesObtenidos = request.responseText;
      procesarClientes();
     }
   }
//Aqui se envia la petición
    request.open("GET", url, true);
    //Aqui envia la petición al servidor.
    request.send();
 }

function procesarClientes(){
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var JSONClientes = JSON.parse(clientesObtenidos);
//  alert(JSONProductos.value[0].ProductName);
//Mediante el documente traemos todo las cacteristicas de mi html
 var divTabla = document.getElementById("divTablaClientes");
 var  tabla = document.createElement("table");
 var tbody = document.createElement("tbody");

 tabla.classList.add("table");
 tabla.classList.add("table-striped");
//genera tabla
 for (var i = 0; i < JSONClientes.value.length; i++) {
   //pinta las lineas de la fila
   var nuevaFila = document.createElement("tr");
//pinta las lineas de la columna
   var columnaNombre = document.createElement("td");
   columnaNombre.innerText = JSONClientes.value[i].ContactName;

   var columnaCiudad = document.createElement("td");
   columnaCiudad.innerText = JSONClientes.value[i].City;
//Se agregan las imagenes de las banderas del arreglo de cada uno de los clientes
   var columnaBandera = document.createElement("td");
   var imgBandera = document.createElement("img");
   imgBandera.classList.add("flag");
   //debemos validar si es londres porque no toma bien el valor
  if (JSONClientes.value[i].Country == "UK") {
    imgBandera.src = rutaBandera + "United-Kingdom.png";
  }
  else {
    imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
  }

    columnaBandera.appendChild(imgBandera);

  nuevaFila.appendChild(columnaNombre);
  nuevaFila.appendChild(columnaCiudad);
  nuevaFila.appendChild(columnaBandera);

  tbody.appendChild (nuevaFila);
 }
  tabla.appendChild (tbody);
  divTabla.appendChild (tabla);
}
