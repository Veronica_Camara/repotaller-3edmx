var productosObtenidos;

 function getProductos() {
   var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
   // para documentar esta libreria envia peticiones HTTP hacia una api
   /*Asi documento vvarios renglones*/
   var request = new XMLHttpRequest();

   request.onreadystatechange = function () {
     // Aqui se valida el estado ! = Preparando 2= Esperando 3 = enviando respuesta
     //4 Respondio y la respuesta de la petición es 200
     if (this.readyState == 4 && this.status == 200) {
/*siempre traer los datos a consola para validar que funciona ok y
se está mandando a una tabla dentro de la consola siempre se debe hacer el jason con parse */
  //    console.table(JSON.parse(request.responseText).value);
  //Mandamos la impresión a mensaje de pantalla y no consola
      productosObtenidos = request.responseText;
      procesarProductos();
     }
   }
//Aqui se envia la petición
    request.open("GET", url, true);
    //Aqui envia la petición al servidor.
    request.send();
 }

function procesarProductos(){
  var JSONProductos = JSON.parse(productosObtenidos);
//  alert(JSONProductos.value[0].ProductName);
//Mediante el documente traemos todo las cacteristicas de mi html
 var divTabla = document.getElementById("divTabla");
 var  tabla = document.createElement("table");
 var tbody = document.createElement("tbody");

 tabla.classList.add("table");
 tabla.classList.add("table-striped");
//genera tabla
 for (var i = 0; i < JSONProductos.value.length; i++) {
   //pinta las lineas de la fila
   var nuevaFila = document.createElement("tr");
//pinta las lineas de la columna
   var columnaNombre = document.createElement("td");
   columnaNombre.innerText = JSONProductos.value[i].ProductName;

   var columnaPrecio = document.createElement("td");
   columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;

   var columnaStock = document.createElement("td");
   columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

  nuevaFila.appendChild(columnaNombre);
  nuevaFila.appendChild(columnaPrecio);
  nuevaFila.appendChild(columnaStock);

  tbody.appendChild (nuevaFila);
 }
  tabla.appendChild (tbody);
  divTabla.appendChild (tabla);
}
